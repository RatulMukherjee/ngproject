 $(document).ready(function() {
     $('#example').DataTable( {
	                    dom: 'Blfrtip',
									
						buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
							
						"scrollX": true,
									 
						"scrollY": true,
						
						"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                                               
                                        } );
} );

