 jQuery(document).ready(function () {
        jQuery('#vmap').vectorMap({
          map: 'world_en',
          backgroundColor: '#ffffff',
          color: '#616161',
          hoverOpacity: 0.7,
          selectedColor: '#4d9ad0',
          enableZoom: true,
          showTooltip: true,
          scaleColors: ['#ffffff', '#ffffff'],
          values: sample_data,
          normalizeFunction: 'polynomial'
        });
      });