 var chart = c3.generate({
    data: {
        x: 'days',
        columns: [
            ['days', '2010-01-01', '2011-01-01', '2012-01-01', '2013-01-01', '2014-01-01', '2015-01-01','2016-02-03'],
            ['Calls', 30, 200, 100, 400, 150, 250,273],
            ['Charges',50,80,200,430,630,70,235],
            
        ],
        types:{
							Calls:'bar',
							Charges:'spline'
						},

						axes:{
							Charges:'y2'
						}
    },
    
    transition:{
						duration:200
					},

					color:{pattern:['#1ebfae','#ffb53e']},

					grid:{
						focus:{
							show:false
						}
					},

					point:{
						focus:{
							expand:{
								r:2
							}
						}
					},
    
    		axis: {
     						 y: {
							    tick: {
                                   values: []
                                       },
  
						        label: {
							          text: 'Calls',
							          position: 'outer-middle'
							        }
						      },
					         y2: {
						        show: true,
						        label: {
							          text: 'Charges',
							          position: 'outer-middle'
							        },
								tick: {
									values:[]
								}	
      						    },
						  x:{
							type: 'timeseries',
							tick:{
							count:7,
								format:'%Y-%m-%d'
							}
						    }
    		}
});

