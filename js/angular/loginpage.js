(function(){
    var app = angular.module('login', []);
    var link = "http://192.168.1.65:8080/Gen/";
    var loginpage = function($scope,$http)
    {  
        $scope.check = {
       value : 'FALSE'
       
     };
        if (localStorage.getItem("e164s")!= null && localStorage.getItem("pass") != null )
            {
                $scope.phonenumber = localStorage.getItem("e164s");
                $scope.password=localStorage.getItem("pass");
            }
        $scope.submit= function()
        {
            var request = $http({
            method: "POST",
            url: link+"login",
            data: $.param({
            e164s: $scope.phonenumber,
            password: $scope.password
        }),
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then( function onSuccess(response)
            {
                $scope.user=response.data;
                //console.log($scope.user);
                if ($scope.user.error == "null" && $scope.user.message == "success") 
                {
                    if ($scope.check.value == "TRUE")
                        {  
                            localStorage.setItem("pass",$scope.password);
                        }
                    localStorage.setItem("e164s",$scope.phonenumber);
                    $http({
                            method: "post",
                            url: link+"getCustomerByE164",
                            data: $.param({
                            e164s: $scope.phonenumber
                        }),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).then( function onSuccess(response)
                            {
                                $scope.user=response.data;
                                if ($scope.user.error == "null" && $scope.user.message == "No Error") 
                                    {
                                        localStorage.setItem("accno",$scope.user.data.accno);
                                        //window.location = "index.html";
                                        console.log("logging in...");
                                    }
                                else if ($scope.user.error == "error")
                                {
                                   $scope.error = $scope.user.message; 
                                }
                                else
                                {
                                    $scope.error = "Error"; 
                                }
                            },
                            function onError(response)
                            {
                              $scope.error="There was an error. Server is offline.";
                            });
                }
                else if ($scope.user.error == "error")
                    {
                        $scope.error = $scope.user.message;
                    }
                else
                {
                  $scope.error = "Error";   
                }
            },
            function onError(response)
            {
               $scope.error="There was an error. Server is offline."; 
            });
        };
        };
    app.controller("loginpage", loginpage);
}());